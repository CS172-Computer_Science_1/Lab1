#include <iostream>
#include <sstream>
#include <string>
#include "lab1_ex01.h" 
#include "lab1_ex08.h" 
#include "lab1_ex15.h"
using namespace std;

int main()
{ 
	int inputN=0;
	string strNotice = "+-=[{(Please enter exercise number (1,8,15).)}]=-+\n";
	
	cout << "####Lab 1 Exercises####\n\n";

	for (int iFor = 0; iFor < 10; iFor++){

		cout << strNotice << endl;
		cin >> inputN;
		cout << "You selected: " << inputN << endl << endl;

		if (inputN == 1){
			cout << "####Lab 1, Exercise 1####\n\n";
			cout << "The total is: " << ex1::total << endl << endl;
		}
		else if (inputN == 8){
			cout << "####Lab 1, Exercise 8####\n\n";
			cout << "Item 1 costs: $" << ex8::i1 << endl;
			cout << "Item 2 costs: $" << ex8::i2 << endl;
			cout << "Item 3 costs: $" << ex8::i3 << endl;
			cout << "Item 4 costs: $" << ex8::i4 << endl;
			cout << "Item 5 costs: $" << ex8::i5 << endl << endl;
			cout << "Your subtotal is: $" << ex8::subt << ", taxes due: $" << ex8::aftertax << ", and your total is: " << ex8::total << endl<<endl;
		}
		else if (inputN == 15){
			cout << "####Lab 1, Exercise 15####\n\n";
			cout << "Let there be a triangle!!!\n\n";
			cout << ex15::megatr<<endl;
		}
		else{
			cout << "Try again!!! \n\n";
			cin.clear();
			cin.ignore();
		}
	}
}