#include <iostream>
using namespace std;

namespace ex8
{
	 double i1 = 15.95, i2 = 24.95, i3 = 6.95, i4 = 12.95, i5 = 3.95,
		subt = i1 + i2 + i3 + i4 + i5,
		aftertax = subt*0.07,
		total = subt + aftertax;
}